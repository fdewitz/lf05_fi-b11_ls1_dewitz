public class Arrays {
    public static void main(String[] args) {
        int[] numbers = {3, 7, 12, 18, 37, 42};
        boolean checkOne = false;
        boolean checkTwo = false;

        for(int i = 0; i <1; i++){
            System.out.println("[ 3 7 12 18 37 42 ]");
        }

        for (int i = 0; i < numbers.length; i++){
            if(numbers[i] == 12){
                checkOne = true;
            }
            else if(numbers[i] == 13){
                checkTwo = true;
            }
        }

        if(checkOne){
            System.out.println("Die Zahl 12 ist in der Ziehung enthalten");
        } else{
            System.out.println("Die Zahl 12 ist nicht in der Ziehung enthalten");
        }

        if(checkTwo){
            System.out.println("Die Zahl 13 ist in der Ziehung enthalten");
        } else{
            System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten");
        }
    }
}
