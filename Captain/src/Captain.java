
public class Captain {

	// Attribute
	private String surname;
	private String name;
	private int captainYears;
	private double gehalt;

	// Konstruktoren

	// TODO: 4. Implementieren Sie einen Konstruktor, der die Attribute surname
	// und name initialisiert.
	public Captain (String surname, String name){
		this.surname = surname;
		this.name = name;
	}
	// TODO: 5. Implementieren Sie einen Konstruktor, der alle Attribute
	// initialisiert.
	public Captain (String surname, String name, int captainYears, double gehalt) {
		this.surname = surname;
		this.name = name;
		this.captainYears = captainYears;
		this.gehalt = gehalt;
	}

	// Verwaltungsmethoden
	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setSalary(double gehalt) {
		if(gehalt >= 0) {
			this.gehalt = gehalt;
		}

	}

	public double getSalary() {
		return this.gehalt;
	}

	public void setCaptainYears (int captainYears) {
		if(captainYears >= 0) {
			this.captainYears = captainYears;
		}
	}
	
	public int getCaptainYears () {
		return this.captainYears;
	}
	// Weitere Methoden
	
	// TODO: 7. Implementieren Sie eine Methode 'vollname', die den vollen Namen
	// (Vor- und Nachname) als string zur�ckgibt.
	public String getFullName () {
		return this.surname + " " + this.name;
	}

	public String toString() {  // overriding the toString() method

		// TODO: 8. Implementieren Sie die toString() Methode.
		return this.surname + " " + this.name + " " + this.captainYears + " " + this.gehalt;
	}

}