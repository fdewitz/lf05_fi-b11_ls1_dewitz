public class TestCaptain {

	public static void main(String[] args) {
		Captain cap1 = new Captain("Jean Luc", "Picard", 2364, 4500.0);
		Captain cap2 = new Captain("Azetbur", "Gorkon", 2373, 0.0);
		Captain cap3 = new Captain("test", "person");
		Captain cap4 = new Captain("test", "person 1");
		Captain cap5 = new Captain("test", "person2");

		cap4.setSalary(3582.53);
		cap5.setSalary(3579.64);

		// Bildschirmausgabe
		System.out.println("Name: " + cap1.getSurname());
		System.out.println("Vorname: " + cap1.getName());
		System.out.println("Kapit�n seit: " + cap1.getCaptainYears());
		System.out.println("Gehalt: " + cap1.getSalary() + " F�derationsdukaten");
		System.out.println("Vollname: " + cap1.getFullName());
		System.out.println(cap1 + "\n"); // Die toString() Methode wird aufgerufen
		System.out.println("\nName: " + cap2.getSurname());
		System.out.println("Vorname: " + cap2.getName());
		System.out.println("Kapit�n seit: " + cap2.getCaptainYears());
		System.out.println("Gehalt: " + cap2.getSalary() + " Darsek");
		System.out.println("Vollname: " + cap2.getFullName());
		System.out.println(cap2 + "\n"); // Die toString() Methode wird aufgerufen

		System.out.println("Voller Name: " + cap3.getFullName());

		System.out.println("Name: " + cap4.getSurname());
		System.out.println("Vorname: " + cap4.getName());
		System.out.println("Kapit�n seit: " + cap4.getCaptainYears());
		System.out.println("Gehalt: " + cap4.getSalary() + " F�derationsdukaten");
		System.out.println("Vollname: " + cap4.getFullName());
		System.out.println("\n"); // Die toString() Methode wird aufgerufen

		System.out.println("Name: " + cap5.getSurname());
		System.out.println("Vorname: " + cap5.getName());
		System.out.println("Kapit�n seit: " + cap5.getCaptainYears());
		System.out.println("Gehalt: " + cap5.getSalary() + " F�derationsdukaten");
		System.out.println("Vollname: " + cap5.getFullName());
		System.out.println("\n"); // Die toString() Methode wird aufgerufen

	}

}