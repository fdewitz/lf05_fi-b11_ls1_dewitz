import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		String bestellung = liestText(myScanner);
        int anzahl = liestGanzeZahl(myScanner);
        double preis = liestDecimalZahl("Geben sie den Nettopreis an: ",myScanner);
        double mwst = liestDecimalZahl("Geben Sie den Mehrwertsetuersatz in Prozent ein: ", myScanner);

        myScanner.close();

        //Verarbeiten
        double gesamtNetto = netto(anzahl, preis);
        double brutto = brutto(gesamtNetto, mwst);

        //
        ausgabe(bestellung, anzahl, gesamtNetto, brutto, mwst);
	}

    public static String liestText(Scanner s){
        System.out.print("Was moechten sie bestellen: ");
        String returnText = s.nextLine();
        return returnText;
    }

    public static int liestGanzeZahl(Scanner s){
        System.out.print("Geben sie die Anzahl an: ");
        int returnNumber = s.nextInt();
        return returnNumber;
    }

    public static double liestDecimalZahl(String text, Scanner s){
        System.out.print(text);
        double returnNumber = s.nextDouble();
        return returnNumber;
    }

    public static double netto(double anzahl, double nettopreis){
        return anzahl * nettopreis;
    }

    public static double brutto(double nettogesamtpreis, double mwst){
        return nettogesamtpreis * (1 + mwst / 100);
    }

    public static void ausgabe(String artikel, int anzahl, double nettogesamtpreis,double bruttogesamtpreis, double mwst){
        System.out.println("\tRechnung");
        System.out.printf("\t\t Netto  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
        System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
    }
}
