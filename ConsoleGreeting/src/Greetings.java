import java.util.Scanner;
public class Greetings {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String name;
        int age;
        
        System.out.println("Hello! What is your name ?");
        name = scanner.nextLine();
        System.out.println("And how old are you?");
        age = scanner.nextInt();
        System.out.println("Hello " + name + "! You are " + age + " years old.");

        scanner.close();
    }
}
