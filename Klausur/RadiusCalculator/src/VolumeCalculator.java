import java.util.Scanner;

public class VolumeCalculator {
    public static void main(String[] args) {
        int radius;
        double erg;
        double z1 = 4;
        double z2 = 3;

        Scanner scanner = new Scanner(System.in);
        radius = scanner.nextInt();
        scanner.close();

        erg = 4.0/3.0 * Math.PI * Math.pow(radius, 3);

        System.out.println("Das ergebnis ist: " + erg);
    }
}
