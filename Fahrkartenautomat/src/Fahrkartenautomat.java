﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
        Scanner tastatur = new Scanner(System.in);
        while(true){
            float zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);
            float rueckgabebetrag;

            // Rückgeldberechnung und -Ausgabe
            // -------------------------------
            if(zuZahlenderBetrag != -1){
            rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);

            tastatur.close();

            fahrkartenAusgeben();

            rueckgabebetrag = rueckgeldAusgeben(rueckgabebetrag);

            System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                                "vor Fahrtantritt entwerten zu lassen!\n"+
                                "Wir wünschen Ihnen eine gute Fahrt." + rueckgabebetrag);
            } else {
                System.out.println("Unzulaessige Anzahl an Fahrkarten. Bitte wählen sie einen Wert zwischen 0 und 10");
            }
        }
    }

    public static float fahrkartenbestellungErfassen(Scanner s){
        System.out.print("Zu zahlender Betrag (EURO): ");
        float zuZahlenderBetrag = s.nextFloat();

        System.out.print("\nWie viele Fahrkarten?");
        float anzahlKarten = s.nextInt();

        if(anzahlKarten == 0){
            anzahlKarten = 1;
        } else if(anzahlKarten <= 0 || anzahlKarten >= 10){
            anzahlKarten = -1;
            zuZahlenderBetrag = 1;
        }

        return zuZahlenderBetrag * anzahlKarten;
    }

    public static float fahrkartenBezahlen(float zuZahlenderBetrag, Scanner s){
        float eingezahlterGesamtbetrag = 0.00f;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
            System.out.println("Noch zu zahlen: " + String.format("%.2f",(zuZahlenderBetrag - eingezahlterGesamtbetrag)) + "\nEingabe (mind. 5Ct, höchstens 2 Euro): ");
    	    float eingeworfeneMünze = s.nextFloat();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        return eingezahlterGesamtbetrag;
    }

    public static void fahrkartenAusgeben(){
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            try {
			    Thread.sleep(250);
		    } catch (InterruptedException e) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
		    }
        }
        System.out.println("\n\n");
    }

    public static float rueckgeldAusgeben(float rueckgabebetrag){
        if(rueckgabebetrag > 0.0)
        {
    	    System.out.println("Der Rückgabebetrag in Höhe von " + String.format("%.2f",rueckgabebetrag) + " EURO");
    	    System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rueckgabebetrag >= 1.9) // 2 EURO-Münzen
            {
        	    System.out.println("2 EURO");
	            rueckgabebetrag -= 2.0;
            }
            while(rueckgabebetrag >= 0.9) // 1 EURO-Münzen
            {
        	    System.out.println("1 EURO");
	            rueckgabebetrag -= 1.0;
            }
            while(rueckgabebetrag >= 0.49) // 50 CENT-Münzen
            {
        	    System.out.println("50 CENT");
	            rueckgabebetrag -= 0.5;
            }
            while(rueckgabebetrag >= 0.19) // 20 CENT-Münzen
            {
        	    System.out.println("20 CENT");
 	            rueckgabebetrag -= 0.2;
            }
            while(rueckgabebetrag >= 0.09) // 10 CENT-Münzen
            {
        	    System.out.println("10 CENT");
	            rueckgabebetrag -= 0.1;
            }
            while(rueckgabebetrag >= 0.049)// 5 CENT-Münzen
            {
        	    System.out.println("5 CENT");
 	            rueckgabebetrag -= 0.05;
            }
        }
        return rueckgabebetrag;
    }
}