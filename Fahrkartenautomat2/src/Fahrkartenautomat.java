import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
        Scanner tastatur = new Scanner(System.in);

        String[] ticketBezeichnungen = {"1: Einzelfahrschein Berlin AB ", "2: Einzelfahrschein Berlin BC", "3: Einzelfahrschein Berlin ABC", "4: Kurzstrecke",
                                    "5: Tageskarte Berlin AB", "6: Tageskarte Berlin BC", "7: Tageskarte Berlin ABC",
                                    "8: Kleingruppen-Tageskarte Berlin AB", "9: Kleingruppen-Tageskarte Berlin BC", "10: Kleingruppen-Tageskarte Berlin ABC"};
        float[] ticketPreise = {2.90f, 3.30f, 3.60f, 1.90f, 8.60f, 9.00f, 9.60f, 23.50f, 24.30f, 24.90f};
        
        for(int i = 0; i < ticketBezeichnungen.length; i++){
            System.out.println(ticketBezeichnungen[i] + ": " + ticketPreise[i]);
        }

        float zuZahlenderBetrag;
       
        zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur, ticketPreise);
        float eingezahlterGesamtbetrag = fahrkartenBezahlen(tastatur, zuZahlenderBetrag);
        fahrkartenAusgeben();
        rueckgeldAusgeben(eingezahlterGesamtbetrag,zuZahlenderBetrag);
        }
    
    public static float fahrkartenbestellungErfassen(Scanner sc, float[] ticketPreise) {
        int anzahl = 0;
        int gesamtAnzahl = 0;
        String kontrolle = "Ja";
        float gesamtpreis = 0;
        while(true){
            System.out.println("Fahrartenwahl: ");
            int fahrkartenWahl = sc.nextInt();
            System.out.println("Anzahl: ");
            anzahl = sc.nextInt();
            gesamtpreis = gesamtpreis + (anzahl * ticketPreise[fahrkartenWahl]);
            gesamtAnzahl = gesamtAnzahl + anzahl;
            if(gesamtAnzahl == 10){ break; }
            System.out.println("Weitere Karte? Ja/Nein");
            kontrolle = sc.next();
            if(kontrolle == "Nein"){ break; }
        }

        return gesamtpreis;
    }

    public static float fahrkartenBezahlen(Scanner tastatur,float zuZahlen) {
	    float eingezahlterGesamtbetrag = 0.0f;
	    while(eingezahlterGesamtbetrag < zuZahlen)
        {	float change = zuZahlen - eingezahlterGesamtbetrag;
       		String output = String.format("%.2f", change);
    	    System.out.println("Noch zu zahlen: " +output+ " Euro");
    	    System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	    float eingeworfeneMünze = tastatur.nextFloat();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
	    return eingezahlterGesamtbetrag;
    }

    public static void fahrkartenAusgeben() {
	    System.out.println("\nFahrschein wird ausgegeben");
	    warte(250);
        System.out.println("\n\n");
    }

    public static void rueckgeldAusgeben(float eingezahlterGesamtbetrag,float zuZahlen) {
	    float rückgabebetrag = eingezahlterGesamtbetrag - zuZahlen;
        if(rückgabebetrag > 0.0)
        {	float backChange = rückgabebetrag;
       		String outputBack = String.format("%.2f", backChange);
    	    System.out.println("Der Rückgabebetrag in Höhe von " + outputBack + " EURO");
    	    System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
        	    System.out.println("2 EURO");
	            rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
        	    System.out.println("1 EURO");
	            rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
        	    System.out.println("50 CENT");
	            rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
        	    System.out.println("20 CENT");
 	            rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
        	    System.out.println("10 CENT");
	            rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
        	    System.out.println("5 CENT");
 	            rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen sie nicht, den Fahrschein\n"+
                          		"vor Fahrtantritt entwerten zu lassen!\n"+
                          		"Wir wünschen ihnen eine gute Fahrt.");
   }

   public static void warte (int millisekunde) {
	   for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");
   }
}
