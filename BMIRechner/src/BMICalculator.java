import java.util.Scanner;

public class BMICalculator {
    public static void main(String[] args) {
        double weight, height, bmi;
        char gender;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Bitte geben sie ihr Geschlecht an: ");
        gender = scanner.nextLine().charAt(0);
        System.out.print("Bitte geben sie ihr Gewicht in Kg an: ");
        weight = scanner.nextFloat();
        System.out.print("Bitte geben sie ihre Größe in Metern an: ");
        height = scanner.nextFloat();
        scanner.close();

        bmi = weight / (height * height);

        if(gender == 'm' && bmi > 25){
            System.out.println("Sie haben übergewicht");
        }
        else if(gender == 'm' && bmi <= 25 && bmi >=20){
            System.out.println("Sie haben Normalgewicht");
        }
        else if(gender == 'm' && bmi < 20){
            System.out.println("Sie haben Untergewicht");
        }
        else if(gender == 'w' && bmi > 24 ){
            System.out.println("Sie haben Übergewicht");
        }
        else if(gender == 'w' && bmi <= 24 && bmi >=19){
            System.out.println("Sie haben Normalgewicht");
        }
        else{
            System.out.println("Sie haben Untergewicht");
        }
    }
}
