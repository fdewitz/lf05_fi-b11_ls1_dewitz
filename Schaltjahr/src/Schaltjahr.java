import java.util.Scanner;

public class Schaltjahr {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Geben sie das Jahr an, welches geprüft werden soll: ");
        int year = scanner.nextInt();
        scanner.close();
        
        if((year%400 == 0 || year%100 == 0) && year%4 == 0 && year >= 1582 && year >= -45){
            System.out.println("Das angegebene Jahr ist ein Schaltjahr laut der Regelung vor und nach 1582");
        }
        else if(year%4 == 0 && year <=1582 && year >= -45){
            System.out.println("Das angegebene Jahr ist ein Schaltjahr laut der Regelung vor aber nicht nach 1582");
        }
        else{
            System.out.println("Das angegebene Jahr ist kein Schaltjahr");
        }
    }
}
