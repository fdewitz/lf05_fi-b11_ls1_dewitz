import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        int start, end, temp, evenNums = 0, unevenNums = 0;
        Scanner sc = new Scanner(System.in);

        System.out.println("Das Programm gibt alle ungerade Zahlen innerhalb eines Intervalls aus.\n");
        do{
            System.out.print("Bitte geben sie eine Zahl ein: ");
            start = sc.nextInt();
            System.out.print("Bitte geben sie eine zweite Zahl ein: ");
            end = sc.nextInt();

            if (end == start) {
                System.out.println("\nDie Zahlen sind gleich. Bitte wählen sie unterschiedliche Zahlen!\n");
            }
        } while (end == start);

        sc.close();

        if( start > end){
            temp = end;
            end = start;
            start = temp;
        }

        for( int i = start; i <= end; i++ ){
            if( i % 2 == 0){
                evenNums++;
            } else {
                unevenNums++;
                System.out.println(i);
            }
        }

        System.out.println("\nDie Anzahl der geraden Zahlen betraegt: " + evenNums);
        System.out.println("Die Anzahl der ungeraden Zahlen betraegt: " + unevenNums);
    }
}
