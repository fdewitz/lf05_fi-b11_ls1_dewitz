import java.util.Scanner;

public class MittelwertMitMethoden {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double smallestNum = 0;
        double biggestNum = 0;
        double input;
        double count = eingabe("Anzahl von Zahlen aus der sie den Mittelwert berechnen wollen", scanner);
        for(int i = 1; i <= count; i++){
            input = eingabe(i + " Zahl", scanner);
            if(smallestNum == 0 && biggestNum == 0){
                smallestNum = input;
                biggestNum = input;
            }
            else if(input > biggestNum){
                biggestNum = input;
            }
            else if(input < smallestNum){
                smallestNum = input;
            }
        }

        scanner.close();
        
        double mittelwert = mittelwert(smallestNum, biggestNum);

        ausgabe(mittelwert);
    }

    public static double eingabe(String valueName, Scanner scanner){
        System.out.print("Bitte geben sie die " + valueName + " ein: ");
        double returnValue = scanner.nextDouble();
        return returnValue;
    }

    public static double mittelwert(double ersteZahl, double zweiteZahl){
        return (ersteZahl + zweiteZahl) / 2.0;
    }

    public static void ausgabe(double ausgabeWert){
        System.out.println("Der Mittelwert betraegt " + ausgabeWert);
    }
}
