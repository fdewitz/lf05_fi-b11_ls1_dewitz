import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class ArrayListProgram {
    public static void main(String[] args) {
        ArrayList<Integer> randNums = new ArrayList<Integer>();
        ArrayList<Integer> indexe = new ArrayList<Integer>();
        Random rand = new Random();
        int[] test = rand.ints(20, 1, 10).toArray();
        for (int i : test) {
            randNums.add(i);
            System.out.println(i);
        }
        
        Scanner sc = new Scanner(System.in);

        System.out.print("Wähle eine Zahl zwischen 1 und 9 zum suchen: ");
        int find = sc.nextInt();
        int count = 0;

        sc.close();

        for(int i = 0; i < randNums.size() - 1; i++) {
            if(randNums.get(i) == find) {
                count++;
                indexe.add(i);
            }
        }

        System.out.println("Die gewählte Zahl kommt " + count + " mal vor.");
        
        for (Integer i : indexe) {
            System.out.println(i);
            randNums.remove(i);
        }

        System.out.println("");

        for (Integer i : randNums) {
            System.out.println(i);
        }

        System.out.println("");

        for (int i = 0; i < randNums.size() - 1; i++) {
            if(randNums.get(i).intValue() == 5){
                Integer addElement = 0;
                randNums.add(i + 1, addElement);
            }
        }

        System.out.println("success");

        for (Integer i : randNums) {
            System.out.println(i);
        }
    }
}
