public class Ladung {
    private String bezeichnung;
    private int menge;

    /**
     * Parameterloser Konstruktor
     */
    public Ladung() {
    }
    /**
     * Parametisierter Konstruktor
     * @param bezeichnung Bezeichnung
     * @param menge Menge
     */
    public Ladung(String bezeichnung, int menge) {
        this.bezeichnung = bezeichnung;
        this.menge = menge;
    }

    
    /** 
     * @return String
     */
    public String getBezeichnung() {
        return this.bezeichnung;
    }

    
    /** 
     * @param bezeichnungNeu Neue Bezeichnung
     */
    public void setBezeichnung(String bezeichnungNeu) {
        this.bezeichnung = bezeichnungNeu;
    }

    
    /** 
     * @return int
     */
    public int getMenge() {
        return this.menge;
    }

    
    /** 
     * @param mengeNeu Neue Menge
     */
    public void setMenge(int mengeNeu) {
        this.menge = mengeNeu;
    }

    
    /**Effekt.: Gibt das Objekt als String zurück. 
     * @return String
     */
    @Override
    public String toString() {
        return "Ladung: " + bezeichnung + " " + menge;
    }

}
