public class App {
    
    /** 
     * @param args
     */
    public static void main(String[] args) {
        Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
        Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
        Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "N'Var");

        klingonen.addLadung(new Ladung("Ferengi Schneckensaft", 200));
        klingonen.addLadung(new Ladung("Bat'leth Klingonen Schwert", 200));

        romulaner.addLadung(new Ladung("Borg-Schrott", 5));
        romulaner.addLadung(new Ladung("Plasma-Waffe", 50));
        romulaner.addLadung(new Ladung("Rote Materie", 2));

        vulkanier.addLadung(new Ladung("Forschungssonde", 35));
        vulkanier.addLadung(new Ladung("Photonentorpedo", 3));

        klingonen.photonentorpedosSchiessen(romulaner);
        romulaner.phaserkanonenSchiessen(klingonen);
        vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
        klingonen.zustandRaumschiffe();
        klingonen.ladungsverzeichnisAusgeben();
        vulkanier.reperaturDurchfuehren(true, true, true, 5);
        vulkanier.photonentorpedosLaden(3);
        vulkanier.ladungsverzeichnisAufraeumen();
        klingonen.photonentorpedosSchiessen(romulaner);
        klingonen.photonentorpedosSchiessen(romulaner);
        klingonen.zustandRaumschiffe();
        klingonen.ladungsverzeichnisAusgeben();
        romulaner.zustandRaumschiffe();
        romulaner.ladungsverzeichnisAusgeben();
        vulkanier.zustandRaumschiffe();
        vulkanier.ladungsverzeichnisAusgeben();

        System.out.println(Raumschiff.eintraegeLogbuchZurueckgeben());
    }
}
