import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {
    private int photonentorpedos;
    private int energieversorgungInProzent;
    private int schildeInProzent;
    private int huelleInProzent;
    private int lebenserhaltungssystemeInProzent;
    private int androidenAnzahl;
    private String schiffsname;
    private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
    private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

    /**
     * Parameterloser Konstruktor
     */
    public Raumschiff() {

    }

    /**
     * Parametisierter Konstruktor
     * @param photonentorpedos Photonentorpedos
     * @param energieversorgungInProzent Energieversorgung in Prozent
     * @param zustandSchildeInProzent Zustand der Schilde in Prozent
     * @param zustandHuelleInProzent Zustand der Hülle in Prozent
     * @param zustandLebenserhaltungssystemeInProzent Zustand der Lebenserhaltungssysteme in Prozent
     * @param anzahlAndroiden Anzahl der Androiden
     * @param schiffsname Schiffsname
     */
    public Raumschiff(int photonentorpedos, int energieversorgungInProzent, int zustandSchildeInProzent,
            int zustandHuelleInProzent, int zustandLebenserhaltungssystemeInProzent, int anzahlAndroiden,
            String schiffsname) {
        this.photonentorpedos = photonentorpedos;
        this.energieversorgungInProzent = energieversorgungInProzent;
        this.schildeInProzent = zustandSchildeInProzent;
        this.huelleInProzent = zustandHuelleInProzent;
        this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
        this.androidenAnzahl = anzahlAndroiden;
        this.schiffsname = schiffsname;
    }

    
    /** 
     * @return int
     */
    public int getPhotonentorpedoAnzahl() {
        return this.photonentorpedos;
    }

    
    /** 
     * @param photonentorpedoAnzahlNeu die neue Anzahl der Photonentorpedos
     */
    public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu) {
        this.photonentorpedos = photonentorpedoAnzahlNeu;
    }

    
    /** 
     * @return int
     */
    public int getEnergieversorgungInProzent() {
        return this.energieversorgungInProzent;
    }

    
    /** 
     * @param zustandEnergieversorgungInProzentNeu die neue Prozentzahl für den Zustand der Energieversorgung.
     */
    public void setEnergieversorgungInProzent(int zustandEnergieversorgungInProzentNeu) {
        this.energieversorgungInProzent = zustandEnergieversorgungInProzentNeu;
    }

    
    /** 
     * @return int
     */
    public int getSchildeInProzent() {
        return this.schildeInProzent;
    }

    
    /** 
     * @param zustandSchildeInProzentNeu die neue Prozentzahl für den Zustand der Schilde
     */
    public void setSchildeInProzent(int zustandSchildeInProzentNeu) {
        this.schildeInProzent = zustandSchildeInProzentNeu;
    }

    
    /** 
     * @return int
     */
    public int gethuelleInProzent() {
        return this.huelleInProzent;
    }

    
    /** 
     * @param zustandHuelleInProzentNeu die neue Prozentzahl für den Zustand der Hülle.
     */
    public void sethuelleInProzent(int zustandHuelleInProzentNeu) {
        this.huelleInProzent = zustandHuelleInProzentNeu;
    }

    
    /** 
     * @return int
     */
    public int getLebenserhaltungsssystemeInProzent() {
        return this.lebenserhaltungssystemeInProzent;
    }

    
    /** 
     * @param zustandLebenserhaltungsssystemeInProzentNeu die neue Prozentzahl für die Lebenserhaltungssysteme
     */
    public void setLebenserhaltungsssystemeInProzent(int zustandLebenserhaltungsssystemeInProzentNeu) {
        this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungsssystemeInProzentNeu;
    }

    
    /** 
     * @return int
     */
    public int getAndroidenAnzahl() {
        return this.androidenAnzahl;
    }

    
    /** 
     * @param androidenAnzahlNeu die Anzahl an Androiden
     */
    public void setAndroidenAnzahl(int androidenAnzahlNeu) {
        this.androidenAnzahl = androidenAnzahlNeu;
    }

    
    /** 
     * @return String
     */
    public String getSchiffsname() {
        return this.schiffsname;
    }

    
    /** 
     * @param schiffsname der Schiffsname
     */
    public void setSchiffsname(String schiffsname) {
        this.schiffsname = schiffsname;
    }

    
    /**
     * Effekt.: Fügt die gegebene Ladung zum Verzeichnis hinzu.
     * @param neueLadung die Ladung welche hinzugefügt werden soll.
     */
    public void addLadung(Ladung neueLadung) {
        ladungsverzeichnis.add(neueLadung);
    }

    
    /**
     * Vorraussetzung.: Bei geladenen Torpedos wird einer auf das gegebene Raumschiff abgeschossen, eine Nachricht an alle ausgegeben und die treffer() Methode ausgeführt.
     * Bei ungeladenen Torpedos wird nur eine Nachricht an alle ausgegeben.
     * @param r das Raumschiff auf das geschossen wird
     */
    public void photonentorpedosSchiessen(Raumschiff r) {
        if (this.photonentorpedos == 0) {
            nachrichtAnAlle("=*Click*=");
        } else {
            this.photonentorpedos--;
            nachrichtAnAlle("Photonentorpedos abgeschossen");
            treffer(r);
        }
    }

    
    /**
     * Vorraussetzung.: Bei einer Energieversorgung über 50 Prozent wird diese um 50 reduziert, eine Nachricht an alle ausgegeben und die treffer() Methode ausgeführt.
     * Bei einer Eneregieversorgung unter 50 Prozent wird nur eine Nachricht an alle ausgegeben.
     * @param r das Raumschiff auf das geschossen wird
     */
    public void phaserkanonenSchiessen(Raumschiff r) {
        if (this.energieversorgungInProzent < 50) {
            nachrichtAnAlle("=*Click*=");
        } else {
            this.energieversorgungInProzent = this.energieversorgungInProzent - 50;
            nachrichtAnAlle("Phaserkanonen abgeschossen");
            treffer(r);
        }
    }

    
    /**
     * Effekt.: Es wird eine Nachricht an alle ausgegeben und je nach Stand der Schilde des gegebenen Schiffes unterschiedliche Attribute reduziert. Bei einer Schildstärke über 0 wird diese um 50 reduziert.
     * Bei einer Schildstärke von 0 und einer Hülle von über 0 wird die Hülle um 50 reduziert. 
     * Wenn sowohl Hülle als auch Schild bei 0 liegen, wird eine weitere Nachricht an alle ausgegeben und die Lebenserhaltungssysteme auf 0 gesetzt. 
     * @param r das Raumschiff das getroffen wurde
     */
    private void treffer(Raumschiff r) {
        nachrichtAnAlle(r.schiffsname + " wurde getroffen!");
        if (r.getSchildeInProzent() > 0) {
            r.setSchildeInProzent(r.getSchildeInProzent() - 50);
        } else if (r.getSchildeInProzent() == 0 && r.huelleInProzent > 0) {
            r.sethuelleInProzent(r.gethuelleInProzent() - 50);
            r.setEnergieversorgungInProzent(r.getEnergieversorgungInProzent() - 50);
        } else if (r.gethuelleInProzent() > 0) {
            r.setLebenserhaltungsssystemeInProzent(0);
            nachrichtAnAlle("Die Lebenserhaltungssysteme der " + r.schiffsname + " wurden zerstoert!");
        }
    }

    
    /**
     * Effekt.: Fügt den gegebenen String zum BroadcastKommunikator hinzu.
     * @param nachricht die zu übergebende Nachricht
     */
    public void nachrichtAnAlle(String nachricht) {
        broadcastKommunikator.add("Nachricht der " + this.schiffsname + ": " + nachricht);
    }

    
    /**
     * Effekt.: Gibt den Broadcastkommunikator zurück.
     * @return ArrayList vom Typ String
     */
    public static ArrayList<String> eintraegeLogbuchZurueckgeben() {
        return broadcastKommunikator;
    }

    
    /**
     * Vorraussetzung.: Wenn eine Ladung mit dem Namen Photonentorpedo vorhanden ist wird die Ladungsmenge mit der gegebenen Zahl verglichen.
     * Wurde eine Zahl die kleiner ist als die Ladungsmenge gegeben werden die Photonentorpedos um diese Zahl erhöht.
     * Wenn eine Zahl gegeben wurde, welche größer oder gleich groß wie die Ladungsmenge ist, wird die gesamte Ladungsmenge zu den Photonentorpedos addiert.
     * In allen Fällen wird eine Nachricht an alle ausgegeben.
     * @param anzahlTorpedos die Zahl an Torpedos die geladen werden sollen
     */
    public void photonentorpedosLaden(int anzahlTorpedos) {
        boolean geladen = false;
        for (Ladung ladung : ladungsverzeichnis) {
            if (ladung.getBezeichnung() == "Photonentorpedo") {
                if (anzahlTorpedos > ladung.getMenge()) {
                    this.photonentorpedos = this.photonentorpedos + ladung.getMenge();
                    ladung.setMenge(0);
                    geladen = true;
                    break;
                } else {
                    this.photonentorpedos = this.photonentorpedos + anzahlTorpedos;
                    ladung.setMenge(ladung.getMenge() - anzahlTorpedos);
                    geladen = true;
                    break;
                }
            }
        }

        if (geladen) {
            nachrichtAnAlle("-=*Click*=-");
        }
    }

    
    /**
     * Vorraussetzung.: Ist die übergebene Anzahl von Androiden größer als die vorhandene Anzahl von Androiden im Raumschiff, dann wird die vorhandene Anzahl von Androiden eingesetzt.
     * Es wird eine Zufallszahl zwischen 0 - 100 erzeugt, welche für die Berechnung der Reparatur benötigt wird.
     * Es wird eine Prozentuale Berechnung ausgeführt und das Ergebnis wird den auf true gesetzten Schiffsstrukturen hinzugefügt.
     * @param schutzschilde boolean Wert der angibt ob die Schutzschilde repariert werden sollen
     * @param energieversorgung boolean Wert der angibt ob die Energieversorgung repariert werden sollen
     * @param huelle boolean Wert der angibt ob die Hülle repariert werden sollen
     * @param anzahlAndroiden Anzahl der zu nutzenden Androiden
     */
    public void reperaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean huelle,
            int anzahlAndroiden) {
        int anzahlStrukturen = 0;

        if (schutzschilde && huelle) {
            anzahlStrukturen = anzahlStrukturen + 3;
        } else if (schutzschilde) {
            anzahlStrukturen = anzahlStrukturen + 1;
        }

        if (anzahlAndroiden > this.androidenAnzahl) {
            anzahlAndroiden = this.androidenAnzahl;
        }

        Random rand = new Random();
        int zuafallszahl = rand.nextInt(100);
        int reperaturBetrag = (zuafallszahl * anzahlStrukturen) / anzahlAndroiden;

        if (schutzschilde && huelle) {
            this.schildeInProzent = this.schildeInProzent + reperaturBetrag;
            this.huelleInProzent = this.huelleInProzent + reperaturBetrag;
            this.energieversorgungInProzent = this.energieversorgungInProzent + reperaturBetrag;
        } else if (schutzschilde) {
            this.schildeInProzent = this.schildeInProzent + reperaturBetrag;
        }
    }

    /**
     * Gibt die derzeitigen Attribute des Raumschiffobjekts zurück.
     */
    public void zustandRaumschiffe() {
        System.out.println("Zustand der " + this.schiffsname + ":\n"
                + "Energieversorgung: " + this.energieversorgungInProzent + " %\n"
                + "Schilde: " + this.schildeInProzent + " %\n"
                + "Huelle: " + this.huelleInProzent + " %\n"
                + "Lebenserhaltungssysteme: " + this.lebenserhaltungssystemeInProzent + " %\n"
                + "Anzahl der Androiden: " + this.androidenAnzahl + "\n"
                + "Anzahl der Photonentorpedos: " + this.photonentorpedos + "\n");
    }

    /**
     * Durchläuft das Ladungsverzeichnis des Raumschiffobjekts und schreibt die einzelnen Ladungen in die Konsole.
     */
    public void ladungsverzeichnisAusgeben() {
        System.out.println("Ladungsverzeichnis der " + this.schiffsname + ": ");
        for (Ladung ladung : this.ladungsverzeichnis) {
            System.out.println(ladung);
        }
        System.out.print("\n");
    }

    /**
     * Durchläuft das Ladungesverzeichnis und entfernt alle Ladungen bei denen die Ladungsmenge gleich Null ist.
     */
    public void ladungsverzeichnisAufraeumen() {
        for (int i = 0; i < this.ladungsverzeichnis.size(); i++) {
            if (ladungsverzeichnis.get(i).getMenge() == 0) {
                this.ladungsverzeichnis.remove(i);
            }
        }
    }
}
