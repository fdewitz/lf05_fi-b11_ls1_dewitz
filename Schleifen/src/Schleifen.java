import java.util.Scanner;

public class Schleifen {
    public static void main(String[] args) {
        //Schreiben Sie ein Programm, dass die Seitenlänge eines Quadrats erfragt und dann im
        //Textmodus ein Quadrat dieser Größe ausgibt.
        Scanner scanner = new Scanner(System.in);
        boolean stopped = false;
        while(!stopped){
            System.out.print("Bitte waehlen sie einen Vorgang:\n\nGeben sie eine 1 ein um ein Quadrat zu erstellen\nGeben sie eine 2 ein um die Quersumme einer Zahl zu bilden\nGeben sie eine 3 ein um die Fakultaet zu berechnen\nGeben sie Stop ein um das Programm zu stoppen\n");
            String option = scanner.next();
            
            if(option.equals("Stop")){
                stopped = true;
            } else if(!option.matches("[1-3]")) {
                System.out.println("Keine Valide Eingabe. Bitte wählen sie eine der vier Optionen\n");
            } else if(Integer.parseInt(option) == 1){
                square(scanner);
                System.out.print("\n");
            } else if (Integer.parseInt(option) == 2){
                checksum(scanner);
                System.out.print("\n");
            } else if (Integer.parseInt(option) == 3){
                faculty(scanner);
                System.out.print("\n");
            } else{
                System.out.println("Keine Valide Eingabe. Bitte wählen sie eine der vier Optionen\n");
            }
        }
        scanner.close();
    }

    public static void square(Scanner scanner) {
        System.out.print("Bitte geben sie die Seitenlaenge ihres Quadrates an: ");
        int length = scanner.nextInt();
        System.out.print("\n");

        for(int i = 1; i <= length; i++){
            if(i == 1 || i == length){
                for(int x = 1; x <= length; x++){
                    System.out.print("* ");
                }
            } else {
                System.out.print("* ");
                for(int x = 2; x < length; x++){
                    System.out.print("  ");
                }
                System.out.print("*");
            }
            System.out.print("\n");
        }
    }

    public static void checksum(Scanner scanner) {
        System.out.println("Bitte geben sie eine Zahl an aus der sie die Quersumme bilden wollen: ");
        String quersumme = scanner.next();
        int i = 0;
        int x = 0;
        int y = 0;
        while(i < quersumme.length()){
            x = quersumme.charAt(i) - '0';
            y = y + x;
            i++;
        }
        System.out.println("Die Quersumme ist: " + y);
    }

    public static void faculty(Scanner scanner) {
        System.out.println("Bitte geben sie eine Zahl kleiner als 20 ein, welche sie berechnen wollen: ");
        int input = scanner.nextInt();
        int i = 0;
        int x = 1;
        if(input <= 20){
            do{
                i++;
                x = x * i;
            } while(i<input);
            System.out.println(String.format("Die Fakultaet von %s ist %s", input, x));
        } else{
            System.out.println("Ihre Zahl war nicht kleiner als 20.");
        }

    }
}
